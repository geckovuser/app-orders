#!/usr/bin/env sh

IMAGE=app-orders

set -e

SCRIPT_DIR=$(dirname "$0")

if [ -z "$GROUP" ] ; then
    echo "Cannot find GROUP env var"
    exit 1
fi

if [ -z "$COMMIT" ] ; then
    echo "Cannot find COMMIT env var"
    exit 1
fi

$(docker -v >/dev/null 2>&1)
if [ $? -eq 0 ]; then
    DOCKER_CMD=docker
else
    DOCKER_CMD=`sudo docker`
fi

CODE_DIR=$(cd $SCRIPT_DIR/..; pwd)
echo $CODE_DIR
$DOCKER_CMD run --rm -v $HOME/.m2:/root/.m2 -v $CODE_DIR:/usr/src/mymaven -w /usr/src/mymaven maven:3.2-jdk-8 mvn -DskipTests package

# cp -r $CODE_DIR/docker $CODE_DIR/target/docker/
# cp -r $CODE_DIR/target/*.jar $CODE_DIR/docker/

REPO=${GROUP}/${IMAGE}
    $DOCKER_CMD build -t ${REPO}:${COMMIT} .;
